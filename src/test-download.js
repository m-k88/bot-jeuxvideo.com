import fs from "fs";
import request from "request";
import { parse } from 'node-html-parser';

const root = parse(`<html lang="fr">
<head>
<title>Bientôt plus de jeux Nintendo… mais pas par Nintendo ? La firme négocierait déjà avec plusieurs studios pour s’occuper de ses franchises  - jeuxvideo.com</title>
<meta charset="utf-8">
<meta name="viewport" id="meta-viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="#FFFFFF" name="theme-color" id="meta-theme-color" />
    <meta name="keywords" content="" />
    <meta name="description" content="Plus les jours passent et plus l&#039;on parle de la nouvelle console de Nintendo, sans même que celle-ci n&#039;ait été dévoilée. Un journaliste révèle dans un dossier que la firme pourrait bien confier ses licences fétiches à des développeurs tiers. Rêves et spéculations
La &quot;Switch 2&quot;, q..." />
    <meta name="robots" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:5" />
            <meta name="news_keywords" content="News jeu,News,News Game,Nintendo Switch,2024" />
                        <link rel="alternate" href="android-app://com.jeuxvideo/jeuxvideo/open/news?code=1843012" />
        <link rel="alternate" href="ios-app://393077232/jeuxvideo/open/news?code=1843012" />
        <meta name="apple-itunes-app" content="app-id=393077232" />
    <meta name="google-site-verification" content="1LxApCLYt6SF4NFRJUP_6nseZQBSGcqZz6LMwii-vr4" />
    <meta property="fb:app_id" content="202446416762"/>
    <meta property="fb:pages" content="30999986172" />
    <meta property="fb:admins" content="576404360"/>
    <meta property="og:site_name" content="Jeuxvideo.com"/>
                                                            <meta property="og:title" content="Bientôt plus de jeux Nintendo… mais pas par Nintendo ? La firme négocierait déjà avec plusieurs studios pour s’occuper de ses franchises" />
                                                                                    <meta property="og:description" content="Plus les jours passent et plus l&#039;on parle de la nouvelle console de Nintendo, sans même que celle-ci n&#039;ait été dévoilée. Un journaliste révèle dans un dossier que la firme pourrait bien confier ses licences fétiches à des développeurs tiers. Rêves et spéculations
La &quot;Switch 2&quot;, q..." />
                                                                                    <meta property="og:image" content="https://image.jeuxvideo.com/medias-crop-1200-675/170492/1704924353-9230-card.jpg" />
                                                                                    <meta property="og:url" content="https://www.jeuxvideo.com/news/1843012/bientot-plus-de-jeux-nintendo-mais-pas-par-nintendo-la-firme-negocierait-deja-avec-plusieurs-studios-pour-s-occuper-de-ses-franchises.htm" />
                                                                                    <meta property="og:type" content="article" />
                                                    <meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@JVCom">
<meta name="twitter:title" content="Bientôt plus de jeux Nintendo… mais pas par Nintendo ? La firme négocierait déjà avec plusieurs studios pour s’occuper de ses franchises ">
<meta name="twitter:description" content="Plus les jours passent et plus l&#039;on parle de la nouvelle console de Nintendo, sans même que celle-ci n&#039;ait été dévoilée. Un journaliste révèle dans un...">
    <meta name="twitter:image" content="https://image.jeuxvideo.com/medias-crop-1200-675/170492/1704924353-9230-card.jpg">
        
    <link rel="icon" type="image/png" href="/favicon.png" sizes="192x192" />
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#FFFFFF">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
</heead><body><div>Macaron!</div><body>`);

//console.log(root.childNodes[0]);

var img = root.querySelector("[property='og:image']");
//var news_keywords = root.querySelector("[name='news_keywords']");
console.log(img._attrs.content);
//console.log(root.querySelector("[property='news_keywords'"));

request.get("https://www.jeuxvideo.com/news/1842943/ps-plus-extra-premium-les-jeux-de-janvier-2024-reveles-des-legendes-dont-un-resident-evil-note-17-20.htm", function(err, res, body){
    console.log('content-type:', res.headers['content-type']);
    console.log('content-length:', res.headers['content-length']);

    if(err != null) {
    	console.error(err);
    } else if(body != null && body != "") {
    	var html = parse(body);
    	var _img = html.querySelector("[property='og:image']");
	console.log(_img._attrs.content);
	var sp = _img._attrs.content.split("/");
	var filePath = sp[sp.length-1];
	download(_img._attrs.content, 'downloads/'+filePath, function() {
		console.log("image has been downloaded? "+fs.existsSync('downloads/'+filePath));
	});
    }
  });

var download = function(uri, filename, callback){
  request.head(uri, function(err, res, body){
    console.log('content-type:', res.headers['content-type']);
    console.log('content-length:', res.headers['content-length']);

    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  });
};

/*download('https://a.thumbs.redditmedia.com/A7qWSEZlRV1IMKm4b-s2MeYmWZwGpOXzh0VWqQ1FoO8.jpg', 'downloads/A7qWSEZlRV1IMKm4b-s2MeYmWZwGpOXzh0VWqQ1FoO8.jpg', function(){
  console.log('done');
});*/

import 'dotenv/config';
import Mastodon from 'mastodon-api'
import fs from "fs";
import request from "request";
import https from 'https';
import prompt from "prompt";
import { parse } from 'node-html-parser';
import { XMLParser, XMLBuilder, XMLValidator} from "fast-xml-parser";
import dbLocal from "db-local";

const { Schema } = new dbLocal({ path: "./databases" });

const DateFetch = Schema("DatePurge", {
	_id: { type: Number, required: true },
	date: { type: Number, default: Date.now(), required: true }
});
const Posts = Schema("Posts", {
  //id: { type: String, required: true },
  title: { type: String, default: "Title", required: true },
  url: { type: String, required: true },
  imagePath: { type: String, default: "" },
  date: { type: Number, required: true },
  tags: { type: Array, requied: true },
  posted: { type: Boolean, required: true }
});
let M = null;

const parser = new XMLParser();
let jObj = null;
var posts = []

var hourCount = 0
var fetchImages = 0

console.log("-----------------------------------")
console.log("starting bot")

if(!fs.existsSync('downloads')) {
	fs.mkdir("downloads", (err) => {
	    if (err) {
	        return console.error(err);
	    }
	    console.log('Directory created successfully!');
	});
}
var dt = DateFetch.find({ _id: 1})
if(dt != null && dt.length > 0) { dt = dt[0]; } else { var _d = DateFetch.create({ _id: 1, date: Date.now() }); _d.save(); dt = _d; }

if(dt != null && !Array.isArray(dt)) {
	if(isPastWeek(dt, Date.now())) {
		var same = null;
		var arr = Posts.find()
		/*for(var i = 0; i < posts.length; i++) {
			var post = Posts.find()
			if(post == same) continue//break;
			if(post != null && !Array.isArray(post) && isPastWeek(post.date, dt)) {
				post.remove()
				same = post;
			}
		}*/
		var d = Date.now();
		for (const e of arr) {
			if(e != null && isPastWeek(e.date, d)) {
				console.log("removed one post: "+e.title);
				e.remove();
			}
		}
		DateFetch.update({ _id: 1 }, { date: Date.now() }).save()
	}
}
var _posts = Posts.find({ posted: false });
if(_posts != null && _posts.length > 0) {
	for(const post of _posts) {
		if(post.title != null && post.url != null) {
		    posts.push({ title: post.title, url: post.url, posted: post.posted, date: post.date });
		}
	}
}

function login() {
	if(M == null && process.env.ACCESS_TOKEN != null) {
		M = new Mastodon({
			client_key: process.env.CLIENT_KEY,
			client_id: process.env.CLIENT_SECRET,
			access_token: process.env.ACCESS_TOKEN,
			timeout_ms: 60*1000,
			api_url: 'https://h4.io/api/v1/'
		});
		/*if(M != null) {
			console.log("Mastodon login: ");
			console.log(M);
		}*/
	}
}

function getFeed() {
	https.get({
  hostname: "www.jeuxvideo.com",
  path: '/rss/rss.xml',
  headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; rv:121.0) Gecko/20100101 Firefox/121.0' }
}, res => {
	  let data = [];
	  const headerDate = res.headers && res.headers.date ? res.headers.date : 'no response date';
	  console.log('Status Code:', res.statusCode);
	  console.log('Date in Response header:', headerDate);

	  res.on('data', chunk => {
	    data.push(chunk);
	  });

	  res.on('end', () => {
	    console.log("data received: ", data);
	    console.log('Response ended: ');

	    jObj = parser.parse(Buffer.concat(data))
	    //console.log(jObj);
	    console.log(jObj.rss);
	    //console.log(jObj.rss.channel.item)
	    if(jObj.rss != null && jObj.rss != {}) {
	    	var d = Date.now();
	    	for(var i in jObj.rss.channel.item) {
	    		var _i = jObj.rss.channel.item[i];
	    		//console.log(_i);
	    		var p = Posts.find({ title: _i.title });
	    		if(p != null && p.length <= 0 || p == null) {
	    			//var d = Date.now();
				var iPath = "";
				var tags = [];
				if(fetchImages < 2) {
					fetchImages++;
					request.get(_i.link, function(err, res, body){
					    console.log('content-type:', res.headers['content-type']);
					    console.log('content-length:', res.headers['content-length']);

					    if(err != null) {
					    	console.error(err);
					    } else if(body != null && body != "") {
					    	var html = parse(body);
					    	var _img = html.querySelector("[property='og:image']");
							//console.log(_img._attrs.content);
							var sp = _img._attrs.content.split("/");
							var filePath = sp[sp.length-1];
							//var tags = [];

							var b = html.querySelector(".bloc-tags-default");
							if(b != null) {
								console.log(b);
								if(b.children != null && b.children.length > 0) {
									for(const t of b.children) {
										if(t.tagName == "A") {
											tags[tags.length] = "#"+t.textContent.replaceAll(" ", "").replaceAll("\n", "").replaceAll("-", ""); //.toLowerCase();
										}
									}
								}
							}
							download(_img._attrs.content, 'downloads/'+filePath, function() {
								console.log("image has been downloaded? "+fs.existsSync('downloads/'+filePath));
								iPath = 'downloads/'+filePath;
								console.log("image saved at:", iPath);
								var p = Posts.find({ title: _i.title });
								console.log(p);
								if(p != null && p.length > 0) {
									p[0].update({ imagePath: iPath, tags: tags }).save();
									console.log("updated post with: "+iPath+" and: "+tags.join(" "));
								}
								var pp = posts.find((e) => e.title == _i.title);
								if(pp != null) {
									pp.imagePath = iPath;
									pp.tags = tags;
									console.log("> updated post!");
								}
							});
					    }
					  });
				}
	    		Posts.create({ title: _i.title, url: _i.link, imagePath: iPath, tags: tags, posted: false, date: d }).save();
	    		posts.push({ title: _i.title, url: _i.link, imagePath: iPath, tags: tags, posted: false, date: d });
			}
		}
	    } else {
	    	console.log("rss is empty!");
	    }
	  });
	}).on('error', err => {
	  console.log('Error: ', err.message);
	});
}

function tick() {
	var d = new Date();
	var hours = d.getHours();
	var mins = d.getMinutes();
	if(hours >= 1 && hours < 7) {
		//console.log("pausé jusqu'à 8h du matin ("+hours+"h"+mins+")");
		return;
	}
	if (mins == "00") {
		hourCount++;
		console.log('Tick ' + hours + "h" + mins);
		if(hourCount >= 2) {
			hourCount = 0
			if(fetchImages >= 2) fetchImages = 0;
			getFeed()
		}
	} else if (mins == "05" || mins == "10" || mins == "15" || mins == "20" || mins == "25"
  || mins == "30" || mins == "35" || mins == "40" || mins == "45" || mins == "50" || mins == "55")  {
		  console.log('Tick ' + hours + "h" + mins);
		  toot();
	}
}
setInterval(tick, 60000);

function toot() {
	if(posts.length > 0) {
		  var post = posts.shift()
		  if(post != null && post.hasOwnProperty("title")) {
		  	login();
		  	if(M != null) {
				if(post.imagePath != null && post.imagePath != "" && fs.existsSync(post.imagePath)) {
					M.post('media', { file: fs.createReadStream(post.imagePath) }).then(resp => {
					  const id = resp.data.id;
					  M.post('statuses', { status: post.title+"\n"+post.url+(post.tags.length > 0 ? "\n"+post.tags.join(" ") : ""), media_ids: [id]/*, visibility: "unlisted"*/ }, (error, data) => {
  					  	if(error) {
  							console.log("post error: ");
  					  		console.error(error);
  					  		posts.push(post);
  					  	} else {
  					  		//console.log(data.content);
  					  		var p = Posts.findOne({ title: post.title });
  					  		if(p != null) {
  					  			p.update({ posted: true }).save();
  					  		}
  							if(post.imagePath != null && post.imagePath != "" /*&& fs.existsSync(post.imagePath)*/) {
  								//fs.unlink(
  								fs.rm(post.imagePath, function() {
  									console.log("isFile: "+post.imagePath+" exist? ", fs.existsSync(post.imagePath))
  								});
  							}
  					  	}
  				  	});
					});
				} else {
				  	M.post('statuses', { status: post.title+"\n"+post.url/*, visibility: "unlisted"*/ }, (error, data) => {
					  	if(error != null) {
							console.log("post error: ");
					  		console.error(error);
					  		posts.push(post);
					  	} else {
					  		//console.log(data.content);
					  		var p = Posts.findOne({ title: post.title });
					  		if(p != null) {
					  			p.update({ posted: true }).save();
					  		}
							if(post.imagePath != null && post.imagePath != "" && fs.existsSync(post.imagePath)) {
								//fs.unlink(
								fs.rm(post.imagePath, function() {
									console.log("isFile: "+post.imagePath+" exist? ", fs.existsSync(post.imagePath))
								});
							}
					  	}
				  	});
				}
		  	} else {
		  		console.log("no login!");
		  	}
		} /*else if(post != null) {
			posts.push(post);
		}*/
	} else {
		console.log("no post remaining in queue!");
	}
}

var download = function(uri, filename, callback){
  request.head(uri, function(err, res, body){
    console.log('content-type:', res.headers['content-type']);
    console.log('content-length:', res.headers['content-length']);

    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  });
}

const WEEK_LENGTH = 604800000;
function isPastWeek(postDate, sd) {
    var date = new Date(sd);
    var lastMonday = new Date(postDate); // Creating new date object for today
    lastMonday.setDate(lastMonday - (lastMonday-1)); // Setting date to last monday
    lastMonday.setHours(0,0,0,0); // Setting Hour to 00:00:00:00


    const res = lastMonday <= date &&
                date > ( lastMonday + WEEK_LENGTH);
    return res; // true / false
}
console.log("-----------------------------------")

prompt.start();
promptCommand()

function promptCommand() {
	prompt.get(['command'], function (err, result) {
    	    if (err) { return console.error(err); }
	    //console.log('Command-line input received:');
	    //console.log('  command: ' + result.command);
	    if(result.command != null) {
	    	if (result.command == "help" || result.command == "") {
	    		console.log("Commands: ");
	    		console.log(" - help | show all commands");
	    		console.log(" - feed | fetch rss/xml");
	    		console.log(" - toot | post one from the queue");
				console.log(" - firstpost | show the first post in the queue");
	    		console.log(" - count | show the number of posts remainings in queue");
	    		//console.log(" - disabled | is posting disabled");
	    	} else if(result.command == "feed") {
	    		getFeed();
	    	} else if(result.command == "toot") {
	    		toot();
	    	} else if(result.command == "firstpost") {
				if(posts.length > 0) {
					console.log(posts[0]);
				} else {
					console.log("no post remaining in queue!");
				}
	    	} else if(result.command == "count") {
	    		console.log("posts remaining in queue: "+posts.length);
	    	}
	    	promptCommand();
	    }
	});
}
